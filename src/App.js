import styles from './App.module.scss';
import Navbar from './Component/Navbar/Navbar';
import Banner from './Component/Banner/Banner'
import Categories from './Component/Categories/Categories';
import Product from './Component/Product/Product';
import Instagram from './Component/Instagram/Instagram';
import Footer from './Component/Footer/Footer'
import Fly from './Component/Fly/Fly';
function App() {
  return (
    <div className={styles.App}>
      <Fly />
      <Navbar />
      <Banner />
      <Categories />
      <Product />
      <Instagram />
      <Footer />
      
    </div>
  );
}

export default App;
