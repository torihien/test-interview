import React from "react";
import styles from './Footer.module.scss'
import { useState,useEffect } from "react";
import Fb from '@iconscout/react-unicons/icons/uil-facebook-f'
import Tw from '@iconscout/react-unicons/icons/uil-twitter'
import Ins from '@iconscout/react-unicons/icons/uil-instagram'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Snc from '@iconscout/react-unicons/icons/uil-snapchat-ghost'
import { Pinterest } from "react-bootstrap-icons";
import payment1 from '../../img/payment-1.png'
import payment2 from '../../img/payment-2.png'
import payment3 from '../../img/payment-3.png'
import payment4 from '../../img/payment-4.png'
import payment5 from '../../img/payment-5.png'
import payment6 from '../../img/payment-6.png'
import payment7 from '../../img/payment-7.png'
import Down from '@iconscout/react-unicons/icons/uil-angle-down'


const Footer = () => {
    const [isSubmit,setIsSubmit] = useState(false)
    const [isClick1, setIsClick1] = useState(false)
    const [isClick2, setIsClick2] = useState(false)
    const [isClick3, setIsClick3] = useState(false)

    const clickHandler1 = () => {
        setIsClick1(!isClick1)
    }
    const clickHandler2 = () => {
        setIsClick2(!isClick2)
    }
    const clickHandler3 = () => {
        setIsClick3(!isClick3)
    }
    // const [windowSize, setWindowSize] = useState(getWindowSize());

    // useEffect(() => {
    //   function handleWindowResize() {
    //     setWindowSize(getWindowSize());
    //   }
  
    //   window.addEventListener('resize', handleWindowResize);
  
    //   return () => {
    //     window.removeEventListener('resize', handleWindowResize);
    //   };
    // }, []);
    // function getWindowSize() {
    //     const {innerWidth, innerHeight} = window;
    //     return {innerWidth, innerHeight};
    //   }
    // const winWidth = windowSize.innerWidth
    const submitHandler = (e) =>{
        e.preventDefault()
        setIsSubmit(true)
    } 
    return (
        <div className = {styles.Footer}>
            <div className={styles.top}>
                <div className={styles.shop}>
                    <h4 onClick ={clickHandler1}>SHOP<span className={styles.icon}><Down /></span></h4>
                    <div className={isClick1 ? styles.info : styles.dpnone}>
                        <p><span>New in</span></p>
                        <p><span>Women</span></p>
                        <p><span>Men</span></p>
                        <p><span>Shoes</span></p>
                        <p><span>Bags & Accessories</span></p>
                        <p><span>Top Brands</span></p>
                        <p><span>Sale & Special Offers</span></p>
                        <p><span>Lookbook</span></p>
                    </div>
                </div>
                <div className={styles.information}>
                    <h4 onClick ={clickHandler2}>INFORMATION<span className={styles.icon}><Down /></span></h4>
                    <div className={isClick2 ? styles.info : styles.dpnone}>
                        <p><span>About</span></p>
                        <p><span>Custormer Service</span></p>
                        <p><span>Reward Program</span></p>
                        <p><span>Shipping & Returns</span></p>
                        <p><span>Privacy Policy</span></p>
                        <p><span>Terms & Conditions</span></p>
                        <p><span>Blog</span></p>
                    </div>
                </div>
                <div className={styles.shop}>
                    <h4 onClick ={clickHandler3}>CUSTORMER SERVICE <span className={styles.icon}><Down /></span></h4>
                    <div className={isClick3 ? styles.info : styles.dpnone}>
                        <p><span>Search Terms</span></p>
                        <p><span>Advanced Search</span></p>
                        <p><span>Order and Returns</span></p>
                        <p><span>Contact Us</span></p>
                        <p><span>Theme FAQs</span></p>
                        <p><span>Consultant</span></p>
                        <p><span>Store Locations</span></p>
                    </div>
                </div>
                <div className={styles.signup}>
                    <h4>NEWSLETTER SIGN UP</h4>
                    <p>Sign up for exclusive updates, new arrivals & insider only discounts</p>
                    <form onSubmit = {submitHandler}>
                        <input type='email' name ='user_email' className={styles.user} placeholder="  Email Address" />
                        <input type='submit' value='SUBMIT' className={styles.bt} />
                    </form> 
                    {isSubmit ? <p> Submit comppleted</p> : <></>}
                    <div className={styles.social}>
                        <span><Fb/></span>
                        <span><Tw/></span>
                        <span><Ins/></span>
                        <span> <Pinterest /></span>
                        <span><Snc/></span>
                    </div>
                </div>
            </div>
            <div className = {styles.bot}>
                <div className = {styles.left}>
                    <p> &copy; 2014 Ella Demo. All Rights Reserved. Powered by Shopfy.</p>
                    <p>Shopify Themes by HaloThemes.com</p>
                </div>
                <div className = {styles.right}>
                    <span><img src={payment1} alt=''/></span>
                    <span><img src={payment2} alt=''/></span>
                    <span><img src={payment3} alt=''/></span>
                    <span><img src={payment4} alt=''/></span>
                    <span><img src={payment5} alt=''/></span>
                    <span><img src={payment6} alt=''/></span>
                    <span><img src={payment7} alt=''/></span>
                    
                </div>
            </div>
        </div>
    )
}

export default Footer
