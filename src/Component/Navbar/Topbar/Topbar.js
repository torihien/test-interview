import React from "react"
import styles from './Topbar.module.scss'
import Bolt from '@iconscout/react-unicons/icons/uil-bolt-alt'

const Topbar = ({content,important,icon}) => {
    
    return (
        <div className = {styles.topbar}>
            <div className = {styles['content-topbar1']}>
                <span><Bolt />{content} <b>{important}</b> </span>
                <span><Bolt />{content} <b>{important}</b></span>
                <span><Bolt />{content} <b>{important}</b></span>
                <span><Bolt />{content} <b>{important}</b></span>
            </div>
            <div className = {styles['content-topbar2']}>
                <span><Bolt />{content} <b>{important}</b> </span>
                <span><Bolt />{content} <b>{important}</b></span>
                <span><Bolt />{content} <b>{important}</b></span>
                <span><Bolt />{content} <b>{important}</b></span>
            </div>
        </div>
    )
}

export default Topbar