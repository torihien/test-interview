import React from "react"
import styles from './Navbar.module.scss'
import Topbar from "./Topbar/Topbar"
import Logo from '../../img/logo_1546508670__55737.original.png'
import ShopBag from '@iconscout/react-unicons/icons/uil-shopping-bag'
import Heart from '@iconscout/react-unicons/icons/uil-heart'
import { useState,useEffect } from "react"
import Toggle from '@iconscout/react-unicons/icons/uil-list-ul'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUser } from "@fortawesome/free-solid-svg-icons"
import { faCartShopping } from "@fortawesome/free-solid-svg-icons"

const Navbar = () => {
    const count = 0;
    const [windowSize, setWindowSize] = useState(getWindowSize());
    const [isClick,setIsClick] = useState(false)
    const clickHandler = () => {
        setIsClick(!isClick)
    }
    useEffect(() => {
      function handleWindowResize() {
        setWindowSize(getWindowSize());
      }
  
      window.addEventListener('resize', handleWindowResize);
  
      return () => {
        window.removeEventListener('resize', handleWindowResize);
      };
    }, []);
    function getWindowSize() {
        const {innerWidth, innerHeight} = window;
        return {innerWidth, innerHeight};
      }
    const winWidth = windowSize.innerWidth

    return (
        <div className={styles.Navbar}>
            <div className={styles.top}>
                <Topbar
                content={`SPECTIAL OFFER: ENJOY 3 MONTHS OF SHOPIFY FOR `} 
                important={`$1/MONTH` } />
            </div>
            
            <div className={winWidth >=1025 ? `${styles.middle}` : `${styles.displayNone}`} >
                <div className={styles.left}>
                    <img src = {Logo} alt='logo' />
                </div>
                <div className={styles.right}>
                    <div className = {styles['top-right']}><h5>FREE SHIPPING ON ALL ORDERS. NO MINIMUM PURCHASE</h5></div>
                    <div className = {styles['bot-right']}>
                        <span><ShopBag /> <span>Shopping Cart</span> <div className={styles['shop-count']}>{count}</div></span>
                        <span><Heart /> <span>My Wish Lists</span></span>
                        <span><span>Sign In </span>  or </span>
                        <span><span>Create an account</span></span>
                    </div>
                </div>
            </div>
            <div className={winWidth >=1025 ? `${styles.bottom}` : `${styles.displayNone}`}>
                <ul>
                    <li>NEW IN</li>
                    <li>TREND</li>
                    <li>COLLECTIONS<div className={styles.promo1}>Sale</div></li>
                    <li>LOOKBOOK</li>
                    <li>DEMO<div className={styles.promo2}>New</div></li>
                    <li>SHOP INSTAGRAM<div className={styles.promo3}>Hot</div></li>
                    <li>BLOG</li>
                    <li>BRANDS</li>
                    <li>FAQS</li>
                </ul>
            </div>
            <div className={winWidth >= 1025 ? `${styles.displayNone}` : `${styles.tb}`}>
                <div className = {styles['tb-middle']}>
                    <div className={styles['tb-left']}>
                        <span onClick = {clickHandler}> <Toggle /></span>
                        <div className = {isClick ? `${styles.menu}` : `${styles.displayNone}`}>
                            <div>NEW IN</div>
                            <div>TREND</div>
                            <div>COLLECTIONS<div className={styles.promo1}>Sale</div></div>
                            <div>LOOKBOOK</div>
                            <div>DEMO<div className={styles.promo2}>New</div></div>
                            <div>SHOP INSTAGRAM<div className={styles.promo3}>Hot</div></div>
                            <div>BLOG</div>
                            <div>BRANDS</div>
                            <div>FAQS</div>
                        </div>
                    </div>
                    <div className={styles['tb-mid']}>
                        <img src = {Logo} alt='logo' />
                    </div>
                    <div className={styles['tb-right']}>
                        <span> <FontAwesomeIcon icon={faUser} /></span>
                        <span> <FontAwesomeIcon icon={faCartShopping} /></span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar