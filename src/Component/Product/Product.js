import React from "react"
import styles from './Product.module.scss'
import Card from "./Card/Card"
import INFOPRODUCT from '../../Data/INFOPRODUCT'
import product1 from '../../img/10-1__27172__51670.1550745367.jpg'


const Product = () => {
    
    return (
        <div className = {styles.Product}>
            <div className={styles.heading}>
                <h5>NEW ARRIVALS</h5>
                <span>View All </span>
            </div>
            <div className={styles['product-list']}>
                {INFOPRODUCT.map((info,key) => {
                    return (
                        <Card 
                        key={key}
                        image1={info.image1} 
                        title={info.title} 
                        name ={info.name} 
                        price = {info.price} 
                        colors = {info.colors} 
                        size={info.size}
                        />
                    )
                })}
            </div>
            <span className={styles.bt}>SHOW MORE</span>
        </div>
    )
}

export default Product