import React from "react"
import styles from './Card.module.scss'
import Heart from '@iconscout/react-unicons/icons/uil-heart'
import { useState} from "react"

const Card = ({image1,title,name,price,colors,size}) => {
    const [sizeName,setSizeName] = useState('')
    const mouseOverHandle = (e) => {
        setSizeName(e.target.innerHTML)
    }
    const createColor = (colors) => {
        if (colors.length <= 5) {
            return (
                <>
                    {colors.map((color,idx) => {
                        return (
                            <span style={{backgroundColor:color}} key ={idx}>
                            </span>
                        ) 
                    })}
                </>
            )
        } else {
            return (
                <>
                    {colors.map((color,idx) => {
                        if(idx <=4) {
                            return (
                                <span style={{backgroundColor:color}} key ={idx}>
                                </span>
                            )
                        }
                    })} <div className={styles['count-color']}>+{colors.length-5}</div>
                </>
            )
        } 
    }

    const createSize = (sizes) =>{
        if (sizes.length > 1) {
            return (
                <div className={styles['add-cart']}>
                    <div className={styles.size}>
                        <div><span>Size:</span><span>{sizeName}</span></div>
                            <div>
                              {sizes.map((size,idx) => {
                                return (
                                    <span key={idx} onMouseOver={mouseOverHandle}>{size}</span>
                                )
                              })}  
                            </div>
                        </div>
                    <span className={styles.bt1}>SUBMIT</span>
                </div>
            )
        } else{
            return (
                <>
                <div className={styles['add-cart2']}>
                    <span className={styles.bt2}>ADD TO CART</span>
                </div>
                </>
            )
        }
    }

    return (
        <div className = {styles.Card}>
            <div className = {styles.image}>
                <img src={image1} alt=''/>
                {createSize(size)}
                <span><Heart /></span>
            </div>
            <div className = {styles.content}>
                <h5>{title}</h5>
                <span>{name}</span>
                <span>{price}</span>
                <span> {createColor(colors)}</span>
            </div>
        </div>
    )
}

export default Card