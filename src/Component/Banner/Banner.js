import React from "react";
import styles from './Banner.module.scss'
import banner1 from '../../img/banner1.jpg'
import banner2 from '../../img/banner2.jpg'
import { motion } from "framer-motion";
import { useState } from "react";
const Banner = () => {
    


    
    return (
        <div className={styles.Banner}> 
            <div className={styles.image}> 
                <img  src={banner1} alt='banner'/>
            </div>
            <div className = {styles['main-product']}>
                <motion.div
                     initial={{ opacity: 0, scale: 0.5 }}
                     whileInView ={{ opacity: 1, scale: 1 }}
                     transition={{ duration: 0.5 }}  
                ><h1>COSMOPOLIS</h1>
                </motion.div>
                <motion.div
                     initial={{ opacity: 0, scale: 0.5 }}
                     whileInView ={{ opacity: 1, scale: 1 }}
                    transition= {{duration: 1}}

                ><p>Quisquemos sodales suscipit tortor ditaemcos condimentum de cosmo lacus meleifend menean diverra loremous.</p>
                </motion.div>
                <motion.div
                     initial={{ opacity: 0, scale: 0.5 }}
                     whileInView ={{ opacity: 1, scale: 1 }}
                    transition= {{duration: 1.5}}
                >
                <span>SHOP NOW</span>
                </motion.div>
            </div>
        </div>
    )
}

export default Banner;