import React from "react";
import styles from './Instagram.module.scss'
import Insta from '@iconscout/react-unicons/icons/uil-instagram'
import {Swiper, SwiperSlide} from "swiper/react"
import {useSwiper} from "swiper/react"
import 'swiper/css'
import insta1 from '../../img/instagram-gallery-1.jpg'
import insta2 from '../../img/instagram-gallery-2.jpg'
import insta3 from '../../img/instagram-gallery-3.jpg'
import insta4 from '../../img/instagram-gallery-4.jpg'
import insta5 from '../../img/instagram-gallery-5.jpg'
import insta6 from '../../img/instagram-gallery-6.jpg'
import insta7 from '../../img/instagram-gallery-7.jpg'
import insta8 from '../../img/instagram-gallery-8.jpg'
import insta9 from '../../img/instagram-gallery-9.jpg'
import insta10 from '../../img/instagram-gallery-10.jpg'
import Left from '@iconscout/react-unicons/icons/uil-angle-left'
import Right from '@iconscout/react-unicons/icons/uil-angle-right'
import SwiperButtonPrev from "./Slidemove/SwiperButtonPrev";
import SwiperButtonNext from "./Slidemove/SwiperButtonNext";
import { useState,useEffect } from "react";
const Instagram = () => {

    const [windowSize, setWindowSize] = useState(getWindowSize());


    function getWindowSize() {
        const {innerWidth, innerHeight} = window;
        return {innerWidth, innerHeight};
      }
      const winWidth = windowSize.innerWidth
      useEffect(() => {
        function handleWindowResize() {
          setWindowSize(getWindowSize());
        }
        window.addEventListener('resize', handleWindowResize);
        return () => {
          window.removeEventListener('resize', handleWindowResize);
        };
      }, []);
    
    return (
        <div className = {styles.Instagram}>
            <h3><span><Insta /> </span> <span> #ELLA ON INSTAGRAM </span></h3>
            <p>Phasellus lorem malesuada ligula pulvinar commodo maecenas</p>
            <div className ={styles.list}>
                <Swiper
                spaceBetween={20}
                slidesPerView = {winWidth > 768 ? (winWidth > 1025 ? (winWidth > 1500 ? (winWidth > 1750 ? 5 : 4 ) : 3 ): 2) : 1}
                grabCursor = {true}
                onSlideChange={()=> console.log('slide change')}
                className = {styles['list-slider']}
                >
                    <SwiperButtonPrev className={styles['icon-left']}><Left /></SwiperButtonPrev>
                    <SwiperButtonNext className={styles['icon-right']}><Right /></SwiperButtonNext>
                    <div id={styles['icon-right']} ><Right /></div>
                    <SwiperSlide>
                        <img src={insta1} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta2} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta3} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta4} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta5} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta6} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta7} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta8} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta9} alt='' />
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={insta10} alt='' />
                    </SwiperSlide>

                </Swiper>

            </div>
            <button className={styles.bt}>VIEW GALLERY</button>
        </div>
    )
}

export default Instagram
