import { useSwiper } from "swiper/react";
import React from "react";

const SwiperButtonNext = ({ children, className }) => {
  const swiper = useSwiper();
  return (
  <div onClick={() => swiper.slideNext()} className={className}>{children}</div>
  );
};

export default SwiperButtonNext