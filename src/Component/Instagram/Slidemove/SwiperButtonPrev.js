import { useSwiper } from "swiper/react";
import React from "react";

const SwiperButtonPrev = ({ children, className }) => {
  const swiper = useSwiper();
  return (
  <div onClick={() => swiper.slidePrev()} className={className}>{children}</div>
  );
};

export default SwiperButtonPrev