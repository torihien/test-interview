import React from "react";
import styles from './Categories.module.scss'
import categorise1 from '../../img/image-collection-59.jpg'
import categorise2 from '../../img/image-collection-63.jpg'
import categorise3 from '../../img/image-collection-12.jpg'
const Categories = () => {
    const categories = [
        {
            image: categorise1, 
            title: `EDITOR'S PICK`,
        },
        {
            image: categorise2, 
            title: 'SHOES',
        },
        {
            image: categorise3, 
            title: `ACCESSORIES`,
        },
    ]


    return (
        <div className ={styles.Categories}>
            {categories.map((category,idx) => {return (
                <div className={styles.cate} key={idx}>
                    <div className = {styles.image} >
                        <img src = {category.image} alt ='' />
                    </div>
                    <div className = {styles.title}>
                        {category.title}
                    </div>
                    <div className={styles.modal}>
                    </div>
                </div>
            )})}
        </div>
    )
}

export default Categories