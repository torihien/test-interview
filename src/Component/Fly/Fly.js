import React from "react";
import styles from  './Fly.module.scss';
import Time from '@iconscout/react-unicons/icons/uil-clock-eight'
import Share from '@iconscout/react-unicons/icons/uil-share-alt'
import ArrowUp from '@iconscout/react-unicons/icons/uil-top-arrow-to-top'
const Fly = () => {
    const clickHandler = () => {
        console.log(window.scrollY)
        window.scrollTo({top: 0,behavior: `smooth`})     
        
    }
    console.log(window)
    return (
        <div className={styles.Fly}>
            <div><Time /></div>
            <div><Share /></div>
            <div onClick={clickHandler}><ArrowUp/></div>
        </div>
    )

}

export default Fly