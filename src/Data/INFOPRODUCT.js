import product11 from '../img/4.1__41003__27986.1550745365.jpg'
import product12 from '../img/3-7__79905__98824.1550745365.jpg'
import product21 from '../img/4-3__82109__44390.1550745374.jpg'
import product22 from '../img/5-2__10249__00415.1550745374.jpg'
import product31 from '../img/6-1__21372__11937.1550745341.jpg'
import product32 from '../img/6-2__52632__75546.1550745341.jpg'
import product41 from '../img/6-5__98868__27813.1550745378.jpg'
import product42 from '../img/6-6__07311__69469.1550745378.jpg'

const INFOPRODUCT = [
    {
        image1: product11,
        image2: product12,
        title:'SOLDING',
        name: 'Naminos Dementus A Milance - Pattern',
        price: '$86.00 - $99.00',
        colors: ['#4b6cc0','#494949','#e8e8e8'],
        size: ['XS','S'],
    },
    {
        image1: product21,
        image2: product22,
        title:'TOMORROW',
        name: 'Dinterdum Pretium Condimento - Pattern',
        price: '$139.00 - $189.00',
        colors: ['#000000','#e8e8e8','#4b6cc0','#13c89c','#fff100','#494949'],
        size: [],
    },
    {
        image1: product31,
        image2:product32,
        title:'PAUL SMITH',
        name: 'Magnis Darturien Metro Lacinis - Black',
        price: '$86.00',
        colors: ['#ffffff'],
        size: ['XS','S','M','L','XL','2X'],
    },
    {
        image1: product41,
        image2:product42,
        title:'DONATELLO',
        name: 'Loremous Saliduar A Cosmopolito - Pattern',
        price: '$139.00 - $189.00',
        colors: ['#494949','#e8e8e8','#13c89c',],
        size: ['XS','S','M','L'],
    },
]

export default INFOPRODUCT;